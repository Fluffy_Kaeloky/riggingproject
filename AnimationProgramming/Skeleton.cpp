#include "Skeleton.h"
#include "Engine.h"
#include "Transform.h"
#include "SkeletonData.h"

Skeleton::Skeleton()
{
}

Skeleton::~Skeleton()
{
	if (initialized)
		Shutdown();
}

auto Skeleton::Init() -> void
{
	if (!initialized)
		RebuildTreeData();
}

auto Skeleton::Shutdown() -> void
{
	if (initialized)
	{
		if (root)
			delete root;
		root = nullptr;

		bones.clear();
	}
}

auto Skeleton::DrawSkeleton() const -> void
{
	Vec3 shift = Vec3(0.0f, -200.0f, 0.0f);

	for (int i = 0; i < (int)bones.size(); i++)
	{
		if (bones.at(i)->GetTransform().GetParent() == nullptr)
			continue;

		Vec3 glbBonePos = bones.at(i)->GetTransform().GlobalPosition();
		Vec3 glbParentBonePos = bones.at(i)->GetTransform().GetParent()->GlobalPosition();

		glbBonePos += shift;
		glbParentBonePos += shift;

		const float* color = bones.at(i)->GetColor();

		DrawLine(glbBonePos.x, glbBonePos.y, glbBonePos.z, glbParentBonePos.x, glbParentBonePos.y, glbParentBonePos.z, color[0], color[1], color[2]);
	}
}

auto Skeleton::SetSkeletonData(SkeletonData* data) -> void
{
	for each (auto b in bones)
	{
		Bone* bone = b.second;
		bone->GetTransform().SetPosition(bone->GetLocalRestPosition());
		bone->GetTransform().SetRotation(bone->GetLocalRestRotation());
	}

	if (data == nullptr)
		return;

	for (int i = 0; i < (int)bones.size(); i++)
	{
		Bone* bone = bones.at(i);
		
		Vec3 relPos = data->GetRelativeTranslation(i);
		Quat relRot = data->GetRelativeRotation(i);

		bone->GetTransform().Rotate(relRot, Space::Local);

		bone->GetTransform().Translate(relPos, Space::Local);
	}
}

auto Skeleton::RebuildTreeData() -> void
{
	bones.clear();
	root = nullptr;

	int boneCount = GetSkeletonBoneCount();
	//Quick fix.
	boneCount = 64;

	for (int i = 0; i < boneCount; i++)
	{
		float color[3];
		color[0] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		color[1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		color[2] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

		bones[i] = new Bone(this, i, GetSkeletonBoneName(i), color);
	}

	root = bones[0];

	for (int i = 0; i < boneCount; i++)
	{
		int index = GetSkeletonBoneParentIndex(i);
		if (index == -1)
			continue;

		bones[i]->GetTransform().SetParent(&bones[index]->GetTransform());
	}

	for (int i = 0; i < boneCount; i++)
	{
		Vec3 pos = Vec3::zero;
		Quat rot = Quat::identity;

		GetSkeletonBoneLocalBindTransform(i, pos.x, pos.y, pos.z, rot.w, rot.x, rot.y, rot.z);

		bones[i]->GetTransform().SetPosition(pos);
		bones[i]->GetTransform().SetRotation(rot);

		bones[i]->SetRestTransformLocal(bones[i]->GetTransform().Position(), bones[i]->GetTransform().Rotation());
		bones[i]->SetRestTransform(bones[i]->GetTransform().GlobalPosition(), bones[i]->GetTransform().GlobalRotation());
	}
}
