#pragma once
#include "math.h"

#include <map>
#include<string>

class Transform;

class SkeletonData
{
public:
	SkeletonData();
	SkeletonData(const char* animName, int boneCount);
	SkeletonData(const char* animName, int keyframe, int boneCount);

	void DefineFrom(const char* animName, int keyframe);
	
	void SetRelativeTranslation(int index, const Vec3& pos);
	void SetRelativeRotation(int index, const Quat& rot);
	void SetTransform(Transform tr, int index);

	inline int GetBoneCount() { return this->boneCount; }

	std::map<int, Transform> GetTransform();
	Vec3 GetRelativeTranslation(int index);
	Quat GetRelativeRotation(int index);

private:

	std::string animName = "none";
	std::map<int, Vec3> skeletonPos;
	std::map<int, Quat> skeletonRot;
	int boneCount = 64;
};