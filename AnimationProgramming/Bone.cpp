#include "Bone.h"
#include "Transform.h"
#include "Skeleton.h"

Bone::Bone(Skeleton* skeleton, int index, const std::string& name, float color[3])
{
	this->skeleton = skeleton;
	this->index = index;
	this->name = name;

	transform = new Transform();

	memcpy(this->color, color, sizeof(float) * 3);
}

auto Bone::SetRestTransform(Vec3 pos, Quat rot) -> void
{
	restPos = pos;
	restRot = rot;
}

auto Bone::SetRestTransformLocal(Vec3 pos, Quat rot) -> void
{
	restPosLocal = pos;
	restRotLocal = rot;
}

auto Bone::GetRotationDelta() const -> Quat
{
	Quat delta = restRot * transform->GlobalRotation().Conjugate();

	return delta;
}

auto Bone::GetPositionDelta() const -> Vec3
{
	Vec3 delta = Vec3::zero;

	delta = transform->GlobalPosition() + transform->GlobalRotation() * restRot.Conjugate() * (-restPos);

	return delta;
}

auto Bone::GetParent() const -> Bone*
{
	int i = GetSkeletonBoneParentIndex(index);
	if (i == -1)
		return nullptr;

	return skeleton->GetBoneFromIndex(i);
}

auto Bone::GetRestPosition() const -> const Vec3&
{
	return restPos;
}

auto Bone::GetRestRotation() const -> const Quat&
{
	return restRot;
}

auto Bone::GetLocalRestPosition() const -> const Vec3 &
{
	return this->restPosLocal;
}

auto Bone::GetLocalRestRotation() const -> const Quat &
{
	return this->restRotLocal;
}

