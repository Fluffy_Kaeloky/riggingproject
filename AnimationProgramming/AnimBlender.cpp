#include "AnimBlender.h"

#include "SkeletonData.h"
#include "Anim.h"

#include <iostream>

AnimBlender::AnimBlender(Anim * animA, Anim * animB)
{
	anim1 = animA;
	anim2 = animB;

	data1 = new SkeletonData();
	data2 = new SkeletonData();
}

AnimBlender::~AnimBlender()
{
	anim1 = nullptr;
	anim2 = nullptr;

	if (data1)
		delete data1;
	data1 = nullptr;

	if (data2)
		delete data2;
	data2 = nullptr;
}

auto AnimBlender::FillSkeletonData(SkeletonData* data) const -> void
{
	anim1->SetSkeletonData(data1);
	anim2->SetSkeletonData(data2);

	if (data1->GetBoneCount() != data2->GetBoneCount())
	{
		std::cout << "Skeletons aren't matching for blending !" << std::endl;
		return;
	}

	for (int i = 0; i < data1->GetBoneCount(); i++)
	{
		data->SetRelativeTranslation(i, Vec3::Lerp(data1->GetRelativeTranslation(i), data2->GetRelativeTranslation(i), interpolationValue));
		data->SetRelativeRotation(i, Quat::Slerp(data1->GetRelativeRotation(i), data2->GetRelativeRotation(i), interpolationValue));
	}
}
