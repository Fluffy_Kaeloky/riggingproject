#include "Math.h"
#include <math.h>
#include <cstring>
#include <cfloat>

// --- ----- --- //

#pragma region Vec3

const Vec3 Vec3::zero = Vec3(0.0f, 0.0f, 0.0f);

auto Vec3::Add(const Vec3 & _v1, const Vec3 & _v2) -> Vec3
{
	return Vec3(_v1.x + _v2.x, _v1.y + _v2.y, _v1.z + _v2.z);
}

auto Vec3::operator+(const Vec3 & other) const -> Vec3
{
	return Vec3::Add(*this, other);
}

auto Vec3::operator+=(const Vec3 & other) -> Vec3 &
{
	*this = Vec3::Add(*this, other);
	return *this;
}

auto Vec3::operator-(const Vec3 & other) const -> Vec3
{
	return Vec3(x - other.x, y - other.y, z - other.z);
}

auto Vec3::Inverted() const -> Vec3
{
	return Vec3(-x, -y, -z);
}

auto Vec3::Lerp(const Vec3 & a, const Vec3 & b, float t) -> Vec3
{
	return a + t * (b - a);
}

#pragma endregion

// --- ----- --- //

#pragma region Quat

const Quat Quat::identity = Quat(1.0f, 0.0f, 0.0f, 0.0f);

auto Quat::ToMat4(float* mat) const -> void
{
	memset(mat, 0, sizeof(float) * 16);

	float sqrX = powf(x, 2.0f);
	float sqrY = powf(y, 2.0f);
	float sqrZ = powf(z, 2.0f);

	//BASIC CODE
	
	mat[0] = 1.0f - 2.0f * sqrY - 2.0f * sqrZ;
	mat[1] = 2.0f * x * y - 2.0f * w * z;
	mat[2] = 2.0f * x * z + 2.0f * w * y;

	mat[4] = 2.0f * x * y + 2.0f * w * z;
	mat[5] = 1.0f - 2.0f * sqrX - 2.0f * sqrZ;
	mat[6] = 2.0f * y * z - 2.0f * w * x;

	mat[8] = 2.0f * x * z - 2.0f * w * y;
	mat[9] = 2.0f * y * z + 2.0f * w * x;
	mat[10] = 1.0f - 2.0f * sqrX - 2.0f * sqrY;

	mat[15] = 1.0f;
}

auto Quat::Multiply(const Quat & other) -> Quat &
{
	float oldw = w;
	float oldx = x;
	float oldy = y;
	float oldz = z;

	w = oldw * other.w - oldx * other.x - oldy * other.y - oldz * other.z;
	x = oldw * other.x + oldx * other.w + oldy * other.z - oldz * other.y;
	y = oldw * other.y - oldx * other.z + oldy * other.w + oldz * other.x;
	z = oldw * other.z + oldx * other.y - oldy * other.x + oldz * other.w;

	return *this;
}

auto Quat::operator*(const Quat & other) const -> Quat
{
	return Quat::Multiply(*this, other);
}

auto Quat::operator*(const Vec3 & other) const -> Vec3
{
	Quat inverted = Quat(w, -x, -y, -z);
	Quat converted = Quat(0.0f, other.x, other.y, other.z);
	Quat copy = Quat(*this);

	Quat raw = copy*converted;
		raw = raw*inverted;

	Vec3 result = Vec3(raw.x, raw.y, raw.z);
	return result;
}

auto Quat::operator == (const Quat& other) const -> bool
{
	return AreEqual(w, other.w) && AreEqual(x, other.x) && AreEqual(y, other.y) && AreEqual(z, other.z);
}

auto Quat::operator*=(const Quat & other) -> Quat &
{
	this->Multiply(other);
	return *this;
}

auto Quat::operator+(const Quat& other) -> Quat
{
	return Quat::Add(*this, other);
}

auto Quat::operator+=(const Quat& other) -> Quat &
{
	*this = Quat::Add(*this, other);
	return *this;
}

auto Quat::FromEuler(float x, float y, float z) -> Quat
{
	Quat q = Quat();

	float rx, ry, rz;

	rx = x * DEG2RAD;
	ry = y * DEG2RAD;
	rz = z * DEG2RAD;

	float c1 = cosf(rx / 2.0f);
	float c2 = cosf(ry / 2.0f);
	float c3 = cosf(rz / 2.0f);

	float s1 = sinf(rx / 2.0f);
	float s2 = sinf(ry / 2.0f);
	float s3 = sinf(rz / 2.0f);

	q.w = c1 * c2 * c3 - s1 * s2 * s3;
	q.z = s1 * s2 * c3 + c1 * c2 * s3;
	q.x = s1 * c2 * c3 + c1 * s2 * s3;
	q.y = -(c1 * s2 * c3 - s1 * c2 * s3);

	return q;
}

auto Quat::Add(const Quat & q1, const Quat & q2) -> Quat
{
	return Quat(q1.w + q2.w, q1.x + q2.x, q1.y + q2.y, q1.z + q2.z).Normalize();
}

auto Quat::Multiply(const Quat & _q1, const Quat & q2) -> Quat
{
	return Quat(Quat(_q1).Multiply(q2));
}

auto Quat::Conjugate() const -> Quat
{
	return Quat(w, -x, -y, -z);
}

auto Quat::ToEuler() const -> Vec3
{
	Vec3 v = Vec3();
	v.z = atan2f(2.0f * x * y + 2.0f * w * z, w * w + x * x - y * y - z * z);
	v.y = asinf(2.0f * w * y - 2.0f * x * z);
	v.x = atan2(2.0f * y * z + 2.0f * w * x, -w * w + x * x + y * y - z * z);

	v.x *= RAD2DEG;
	v.y *= RAD2DEG;
	v.z *= RAD2DEG;

	return v;
}

auto Quat::ToString() const -> std::string
{
	std::string str = "";
	str += "(w:" + std::to_string(w) + ", x:" + std::to_string(x) + ", y:" + std::to_string(y) + ", z:" + std::to_string(z) + ")";

	return str;
}

auto Quat::Normalize() -> Quat &
{
	float norm = sqrtf(w * w + x * x + y * y + z * z);
	norm = 1.0f / norm;

	w *= norm;
	x *= norm;
	y *= norm;
	z *= norm;

	return *this;
}

auto Quat::Lerp(const Quat & a, const Quat & b, float t) -> Quat
{
	Quat q = (1.0f - t) * a + t * b;
	return q.Normalize();
}

auto Quat::Dot(const Quat & a, const Quat & b) -> float
{
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

auto Quat::Slerp(const Quat & a, const Quat & b, float t) -> Quat
{
	float dotProduct = Quat::Dot(a, b);
	Quat q1 = Quat(a);
	if (dotProduct < 0.f)
	{
		q1 = -1 * q1;
		dotProduct *= -1;
	}

	float sinO = sqrtf(1 - (-dotProduct));
	Quat q = (sinO*(1 - t) / sinO)*q1 + ((sinO * t) / sinO) * b;
	return q.Normalize();
}

auto Quat::Divide(const Quat & a, const Quat & b) -> Quat
{
	Quat q;
	
	float divided = powf(b.w, 2.f) + powf(b.x, 2.f) + powf(b.y, 2.f) + powf(b.z, 2.f);
	
	q.w = (b.w*a.w + b.x*a.x + b.y*a.y + b.z*a.z) / divided;
	q.x = (b.w*a.x - b.x*a.w - b.y*a.z + b.z*a.y) / divided;
	q.w = (b.w*a.y + b.x*a.z - b.y*a.x - b.z*a.x) / divided;
	q.w = (b.w*a.z - b.x*a.y + b.y*a.x - b.z*a.w) / divided;

	return q;
}

#pragma endregion

// --- ----- --- //

#pragma region Misc

auto operator*(float s, const Vec3& v) -> Vec3
{
	return Vec3(v.x * s, v.y * s, v.z * s);
}

auto operator*(float s, const Quat& q) -> Quat
{
	return Quat(q.w * s, q.x * s, q.y * s, q.z * s);
}

auto AreEqual(float a, float b, float tol) -> bool
{
	return fabsf(a - b) <= tol;
}

auto Mat4Equal(const float* a, const float* b) -> bool
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (AreEqual(a[i + j * 4],b[i + j * 4]))
				return false;
		}
	}

	return true;
}
#pragma endregion
