#pragma once
#include <string>

#include "UnitTestExport.h"

class Anim;
class SkeletonData;

class AnimBlender
{
public:
	AnimBlender() = delete;
	///Weak pointers to animations, they are yours to destroy.
	AnimBlender(Anim* animA, Anim* animB);
	~AnimBlender();

	DECLSPECDEF auto FillSkeletonData(SkeletonData* data) const -> void;
	DECLSPECDEF inline auto SetInterpolationValue(float value) { interpolationValue = value; }

private:
	float interpolationValue = 0.0f;

	Anim* anim1;
	Anim* anim2;

	SkeletonData* data1;
	SkeletonData* data2;
};