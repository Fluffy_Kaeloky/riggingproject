// AnimationProgramming.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Engine.h"
#include "Simulation.h"

#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

#include "Math.h"
#include "Skeleton.h"
#include "Anim.h"
#include "Transform.h"
#include "AnimBlender.h"
#include "SkeletonData.h"

auto PrintMatrix4(const float* mat) -> void;

class CSimulation : public ISimulation
{
private:

	Skeleton* jack = nullptr;
	Anim* anim1 = nullptr;
	Anim* anim2 = nullptr;
	AnimBlender* animBlender = nullptr;

	SkeletonData skeletonData;

	int boneCount = 0;

	float* skinMats = nullptr;

	float angle = 0.0f;

public:
	virtual ~CSimulation() override
	{
		if (skinMats)
			delete skinMats;
		skinMats = nullptr;

		if (animBlender)
			delete animBlender;
		animBlender = nullptr;

		if (anim1)
			delete anim1;
		anim1 = nullptr;
		
		if (anim2)
			delete anim2;
		anim2 = nullptr;
	}

	virtual void Init() override
	{
		jack = new Skeleton();
		jack->Init();

		int spine01 =	GetSkeletonBoneIndex("spine_01");
		int spineParent = GetSkeletonBoneParentIndex(spine01);
		const char* spineParentName = GetSkeletonBoneName(spineParent);

		float posX, posY, posZ, quatW, quatX, quatY, quatZ;
		size_t keyCount = GetAnimKeyCount("ThirdPersonWalk.anim");
		GetAnimLocalBoneTransform("ThirdPersonWalk.anim", spineParent, keyCount / 2, posX, posY, posZ, quatW, quatX, quatY, quatZ);
		
		printf("Spine parent bone : %s\n", spineParentName);
		printf("Anim key count : %ld\n", keyCount);
		printf("Anim key : pos(%.2f,%.2f,%.2f) rotation quat(%.10f,%.10f,%.10f,%.10f)\n", posX, posY, posZ, quatW, quatX, quatY, quatZ);

		keyCount = GetAnimKeyCount("ThirdPersonRun.anim");
		printf("Anim key count : %ld\n", keyCount);

		boneCount = GetSkeletonBoneCount();
		boneCount = 64;
		for (int i = 0; i < boneCount; i++)
		{
			printf("// --- Bone : \"%s\"\n", GetSkeletonBoneName(i));
			Vec3 pos;
			GetSkeletonBoneLocalBindTransform(i, pos.x, pos.y, pos.z, quatW, quatX, quatY, quatZ);
			printf("%.2f, %.2f, %.2f\n", pos.x, pos.y, pos.z);
			printf("rotation quat(%.5f, %.5f, %.5f, %.5f)\n", quatW, quatX, quatY, quatZ);
			int parentIndex = GetSkeletonBoneParentIndex(i);
			printf("Parent : \"%d\"\n", GetSkeletonBoneParentIndex(i));
			printf("Index : %d\n", i);
		}

		skinMats = new float[boneCount * 16];

		anim1 = new Anim("ThirdPersonWalk.anim", 1.0f);
		anim2 = new Anim("ThirdPersonRun.anim", 1.0f);
		
		animBlender = new AnimBlender(anim1, anim2);

		anim1->Start();
		anim2->Start();
	}

	virtual void Update(float frameTime) override
	{
		static float frameCount = 0;
		frameCount += 0.01f;

		animBlender->SetInterpolationValue(fabsf(cosf(frameCount)));

		// X axis
		DrawLine(0, 0, 0, 100, 0, 0, 1, 0, 0);

		// Y axis
		DrawLine(0, 0, 0, 0, 100, 0, 0, 1, 0);

		// Z axis
		DrawLine(0, 0, 0, 0, 0, 100, 0, 0, 1);

		animBlender->FillSkeletonData(&skeletonData);
		jack->SetSkeletonData(&skeletonData);

		for (int i = 0; i < boneCount; i++)
		{
			Quat qDelta = jack->GetBoneFromIndex(i)->GetRotationDelta();
			Vec3 pDelta = jack->GetBoneFromIndex(i)->GetPositionDelta();

			Mat4 mat;

			qDelta.ToMat4(mat.values);

			mat[12] = pDelta.x;
			mat[13] = pDelta.y;
			mat[14] = pDelta.z;

			for (int j = 0; j < 16; j++)
				skinMats[i * 16 + j] = mat[j];
		}

		SetSkinningPose(skinMats, boneCount);
		
		if (jack)
			jack->DrawSkeleton();
	}
};

int main()
{
	CSimulation simulation;
	Run(&simulation, 1400, 800);

    return 0;
}

auto PrintMatrix4(const float* mat) -> void
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			std::cout << mat[j + i * 4] << ", ";
			if (j == 4)
				std::cout << std::endl;
		}
	}
}

