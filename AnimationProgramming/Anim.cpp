#include "Anim.h"
#include "Engine.h"
#include "Skeleton.h"
#include "SkeletonData.h"
#include "Transform.h"

#include<ctime>
#include <chrono>
#include <iostream>
#include <algorithm>

const float Anim::animTimestepMs = 5.0f;

Anim::Anim(std::string animName, float animTime)
{
	this->animName = animName;
	this->animFrameCount = GetAnimKeyCount(animName.c_str());
	this->animTime = animTime;
	this->animFrameTime = animTime / (float)animFrameCount;
}

Anim::~Anim()
{
	stopThread = true;
	thread.join();
}

auto Anim::Start() -> void
{
	stopThread = false;
	thread = std::thread(&Anim::Loop, this);
}

auto Anim::Stop() -> void
{
	stopThread = true;
	thread.join();
}

auto Anim::SetSkeletonData(SkeletonData * skeleton) -> void
{
	int curFrame = std::min((int)((time / animTime) * (float)animFrameCount), animFrameCount - 1);

	int boneCount = GetSkeletonBoneCount();
	//Quick fix.
	boneCount = 64;

	float rest;
	float interpValue = modf(time / animFrameTime, &rest);

	for (int i = 0; i < boneCount; i++)
	{
		Vec3 animPos = Vec3();
		Quat animRot = Quat();

		Vec3 nextAnimPos = Vec3();
		Quat nextAnimRot = Quat();
		
		GetAnimLocalBoneTransform(animName.c_str(), i, curFrame, animPos.x, animPos.y, animPos.z, animRot.w, animRot.x, animRot.y, animRot.z);
		GetAnimLocalBoneTransform(animName.c_str(), i, (curFrame == animFrameCount - 1 ? 0 : curFrame + 1), nextAnimPos.x, nextAnimPos.y, nextAnimPos.z, nextAnimRot.w, nextAnimRot.x, nextAnimRot.y, nextAnimRot.z);

		Vec3 finalPos = Vec3::Lerp(animPos, nextAnimPos, interpValue);
		Quat finalRot = Quat::Slerp(animRot, nextAnimRot, interpValue);

		skeleton->SetRelativeTranslation(i, finalPos);
		skeleton->SetRelativeRotation(i,finalRot);
	}
}

auto Anim::SetPlaybackTime(float time) -> void
{
	this->time = time;
}

 auto Anim::StartBlending(std::string _toBlend, float _time) -> void
{

}

auto Anim::Loop() -> void
{
	using namespace std::chrono_literals;

	while (!stopThread)
	{
		auto start = std::chrono::high_resolution_clock::now();
		std::this_thread::sleep_for(1ms);
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<float, std::milli> elapsed = end - start;

		time += elapsed.count() / 1000.0f;

		if (time > animTime)
			time = 0.0f;
	}
}
