#pragma once

#include <map>
#include <vector>
#include "Bone.h"
#include "UnitTestExport.h"

class SkeletonData;

class Skeleton
{
public:
	Skeleton();
	~Skeleton();

	DECLSPECDEF auto Init() -> void;
	DECLSPECDEF auto Shutdown() -> void;

	DECLSPECDEF auto DrawSkeleton() const -> void;

	DECLSPECDEF inline auto GetBoneFromIndex(int i) const->Bone* { return bones.at(i); }

	DECLSPECDEF auto SetSkeletonData(SkeletonData* data) -> void;

private:
	auto RebuildTreeData() -> void;

private:
	bool initialized = false;

	Bone* root = nullptr;
	std::map<int, Bone*> bones = std::map<int, Bone*>();
};