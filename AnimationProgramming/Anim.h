#pragma once
#include "UnitTestExport.h"
#include "Math.h"

#include <string>
#include <thread>

class Skeleton;
class SkeletonData;

class Anim
{
public:
	static const float animTimestepMs;

	Anim() = delete;
	Anim(std::string animName, float animTime);

	~Anim();

	DECLSPECDEF auto Start() -> void;
	DECLSPECDEF auto Stop() -> void;
			    
	DECLSPECDEF auto SetSkeletonData(SkeletonData* skeleton) -> void;
	DECLSPECDEF auto SetPlaybackTime(float time) -> void;

	DECLSPECDEF auto StartBlending(std::string _toBlend, float _blendtime) -> void;

private:
	auto Loop() -> void;

	std::string animName = "";
	float animTime = 1.0f;
	int animFrameCount = -1;
	float animFrameTime = 0.01f;

	float time = 0.0f;

	bool stopThread = false;
	std::thread thread;
};