#include "SkeletonData.h"

#include "Engine.h"
#include "Transform.h"


SkeletonData::SkeletonData()
{
}

SkeletonData::SkeletonData(const char * animName, int boneCount)
{
	this->animName = animName;
	boneCount = boneCount;
}

SkeletonData::SkeletonData(const char * animName, int keyframe, int boneCount)
{
	this->boneCount = boneCount;
	this->DefineFrom(animName, keyframe);
}

void SkeletonData::DefineFrom(const char * animName, int keyframe)
{
	this->animName = animName;

	for (int i = 0; i < boneCount; i++)
	{
		Vec3 pos;
		Quat rot;

		GetAnimLocalBoneTransform(animName, i, keyframe, pos.x, pos.y, pos.z, rot.w, rot.x, rot.y, rot.z);

		this->skeletonPos[i] = pos;
		this->skeletonRot[i] = rot;
	}
}

void SkeletonData::SetRelativeTranslation(int index, const Vec3& pos)
{
	this->skeletonPos[index] = pos;
}

void SkeletonData::SetRelativeRotation(int index, const Quat& rot)
{
	this->skeletonRot[index] = rot;
}

void SkeletonData::SetTransform(Transform tr, int index)
{
	this->SetRelativeRotation(index, tr.Rotation());
	this->SetRelativeTranslation(index, tr.Position());
}

std::map<int, Transform> SkeletonData::GetTransform()
{
	std::map<int, Transform> transform;
	
	for (int i = 0; i < boneCount; i++)
	{
		Transform cur;
		cur.SetPosition(this->skeletonPos[i]);
		cur.SetRotation(this->skeletonRot[i]);
		transform[i] = cur;
	}
	
	return transform;
}

Quat SkeletonData::GetRelativeRotation(int index)
{
	return this->skeletonRot[index];
}

Vec3 SkeletonData::GetRelativeTranslation(int index)
{
	return this->skeletonPos[index];
}
