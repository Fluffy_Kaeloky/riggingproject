#pragma once
#include "Math.h"
#include "UnitTestExport.h"

#include <vector>

enum class Space
{
	World,
	Local
};

class Transform
{
public:
	DECLSPECDEF auto SetPosition(const Vec3&) -> void;
	DECLSPECDEF auto SetRotation(const Quat&) -> void;
			    
	DECLSPECDEF auto GlobalPosition() -> const Vec3&;
	DECLSPECDEF auto GlobalRotation() -> const Quat&;
			    
	DECLSPECDEF inline auto Position() -> const Vec3&{ return position; }
	DECLSPECDEF inline auto Rotation() -> const Quat&{ return rotation; }

	DECLSPECDEF inline auto Translate(const Vec3& t, Space space = Space::World) -> void;
	DECLSPECDEF auto Rotate(const Quat& q, Space space = Space::World) -> void;
			    
	DECLSPECDEF auto ToMat4() const -> Mat4;
			    
	//Does not support transform movement (Can't remove child, because I'm lazy like that).
	DECLSPECDEF auto AddChildren(Transform*) -> void;
	DECLSPECDEF auto SetParent(Transform*) -> void;

	DECLSPECDEF inline auto GetParent() const -> Transform* const { return parent; }

private:
	inline auto SetDirty() -> void { globalCoordinatesDirty = true; for each (auto t in children) t->SetDirty(); }

	auto RecomputeGlobalCoordinates() -> void;

	Transform* parent = nullptr;
	std::vector<Transform*> children = std::vector<Transform*>();

	bool globalCoordinatesDirty = true;

	Vec3 position = Vec3::zero;
	Quat rotation = Quat::identity;

	Vec3 globalPosition = Vec3::zero;
	Quat globalRotation = Quat::identity;
};