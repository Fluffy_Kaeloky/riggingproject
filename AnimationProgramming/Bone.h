#pragma once

#include <list>
#include "Math.h"
#include "Engine.h"

class Transform;
class Skeleton;

class Bone
{
public:
	Bone() = delete;
	Bone(Skeleton* skeleton, int index, const std::string& name, float color[3]);

	inline auto GetTransform() const -> Transform& { return *transform; }

	inline auto GetColor() const -> const float* { return color; }

	auto SetRestTransform(Vec3 pos, Quat rot) -> void;
	auto SetRestTransformLocal(Vec3 pos, Quat rot) -> void;

	auto GetRotationDelta() const -> Quat;
	auto GetPositionDelta() const -> Vec3;

	auto GetParent() const->Bone*;

	auto GetRestPosition() const -> const Vec3&;
	auto GetRestRotation() const -> const Quat&;

	auto GetLocalRestPosition() const -> const Vec3&;
	auto GetLocalRestRotation() const -> const Quat&;


private:
	Skeleton* skeleton = nullptr;

	Transform* transform;

	Vec3 restPos = Vec3::zero;
	Quat restRot = Quat::identity;

	Vec3 restPosLocal = Vec3::zero;
	Quat restRotLocal = Quat::identity;


	int index = -1;
	std::string name = "none";
	
	float color[3];
};