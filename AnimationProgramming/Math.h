#pragma once
#include <string>
#include "UnitTestExport.h"

#define RAD2DEG 57.2957795131f
#define DEG2RAD 0.01745329251f

struct Mat4
{
public:
	float values[16];

	Mat4()
	{
		memset(values, 0, sizeof(float) * 16);

		values[0] = 1.0f;
		values[5] = 1.0f;
		values[10] = 1.0f;
		values[15] = 1.0f;
	}

	Mat4(float v[16])
	{
		memcpy(values, v, sizeof(float) * 16);
	}

	inline auto operator[] (int i) -> float& { return values[i]; }
	inline auto operator[] (int i) const -> const float& { return values[i]; }
};

struct Vec3
{
public:
	static const Vec3 zero;

	float x, y, z;

	Vec3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vec3(float _x, float _y, float _z)
	{
		this->x = _x;
		this->y = _y;
		this->z = _z;
	}

	DECLSPECDEF static auto Add(const Vec3& _v1, const Vec3& _v2) -> Vec3;

	DECLSPECDEF auto operator +(const Vec3& other) const -> Vec3;

	DECLSPECDEF auto operator += (const Vec3& other) -> Vec3&;

	DECLSPECDEF auto operator -(const Vec3& other) const -> Vec3;

	DECLSPECDEF inline auto operator-() const -> Vec3 { return this->Inverted(); }

	DECLSPECDEF auto Inverted() const -> Vec3;

	DECLSPECDEF static auto Lerp(const Vec3& a, const Vec3& b, float t) -> Vec3;
};

DECLSPECDEF auto operator * (float, const Vec3&) -> Vec3;

struct Quat
{
public:
	static const Quat identity;

	float w, x, y, z;

	Quat()
	{
		w = 1.0f;
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	Quat(float w, float x, float y, float z)
	{
		this->w = w;
		this->x = x;
		this->y = y;
		this->z = z;
	}

	DECLSPECDEF auto ToMat4(float* mat) const -> void;
			   
	DECLSPECDEF auto Multiply(const Quat& other) -> Quat&;
			    
	DECLSPECDEF auto operator *(const Quat& other) const -> Quat;
			    
	DECLSPECDEF auto operator *(const Vec3& other) const -> Vec3;

	DECLSPECDEF auto operator == (const Quat& other) const -> bool;

	DECLSPECDEF auto operator *= (const Quat& other) -> Quat&;

	DECLSPECDEF auto operator + (const Quat& other) -> Quat;

	DECLSPECDEF auto operator += (const Quat& other) -> Quat&;

	DECLSPECDEF static auto FromEuler(float x, float y, float z) -> Quat;
			    
	DECLSPECDEF static auto Add(const Quat& q1, const Quat& q2) -> Quat;

	DECLSPECDEF	static auto Multiply(const Quat& _q1, const Quat& q2) -> Quat;
	
	DECLSPECDEF	auto Conjugate() const -> Quat;

	DECLSPECDEF auto ToEuler() const -> Vec3;
			    
	DECLSPECDEF auto ToString() const -> std::string;

	DECLSPECDEF auto Normalize() -> Quat& ;

	DECLSPECDEF static auto Lerp(const Quat& a, const Quat& b, float t) -> Quat;

	DECLSPECDEF static auto Dot(const Quat& a, const Quat& b) -> float;

	DECLSPECDEF static auto Slerp(const Quat& a, const Quat& b, float t) -> Quat;

	DECLSPECDEF static auto Divide(const Quat& a, const Quat& b)->Quat;
};

auto DECLSPECDEF operator * (float, const Quat&) -> Quat;

auto DECLSPECDEF AreEqual(float a, float b, float tol = FLT_EPSILON) -> bool;

auto DECLSPECDEF Mat4Equal(const float* a, const float* b) -> bool;


