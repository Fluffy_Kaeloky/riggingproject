#include "Transform.h"

auto Transform::SetPosition(const Vec3& value) -> void
{
	position = value;
	SetDirty();
}

auto Transform::SetRotation(const Quat& value) -> void
{
	rotation = value;
	SetDirty();
}

auto Transform::GlobalPosition() -> const Vec3&
{
	if (globalCoordinatesDirty)
		RecomputeGlobalCoordinates();
	return globalPosition;
}

auto Transform::GlobalRotation() -> const Quat&
{
	if (globalCoordinatesDirty)
		RecomputeGlobalCoordinates();
	return globalRotation;
}

auto Transform::Translate(const Vec3& t, Space space) -> void
{
	switch (space)
	{
	case Space::World:
		position += t;
		break;
	case Space::Local:
		position += GlobalRotation() * t;
		break;
	}

	SetDirty();
}

auto Transform::Rotate(const Quat & q, Space space) -> void
{
	switch (space)
	{
	case Space::World:
		rotation = q * rotation;
		break;
	case Space::Local:
		rotation *= q;
		break;
	}

	SetDirty();
}

auto Transform::ToMat4() const -> Mat4
{
	Mat4 mat;

	rotation.ToMat4(mat.values);
	mat[12] = position.x;
	mat[13] = position.y;
	mat[14] = position.z;

	return mat;
}

auto Transform::AddChildren(Transform* t) -> void
{
	if (t != nullptr)
		children.push_back(t);

	t->parent = this;

	t->SetDirty();
}

auto Transform::SetParent(Transform* t) -> void
{
	t->AddChildren(this);
}

auto Transform::RecomputeGlobalCoordinates() -> void
{
	if (parent)
	{
		if (parent->globalCoordinatesDirty)
			parent->RecomputeGlobalCoordinates();
	}

	Quat parentRotation = (parent != nullptr ? parent->globalRotation : Quat::identity);
	Vec3 parentPosition = (parent != nullptr ? parent->globalPosition : Vec3::zero);

	globalRotation = parentRotation * rotation;
	globalPosition = parentPosition + (parentRotation * position);
}
