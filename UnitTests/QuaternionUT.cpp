#include "stdafx.h"
#include "CppUnitTest.h"

#include <locale>
#include <codecvt>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>

#include "../AnimationProgramming/Math.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(QuaternionUT)
	{
	public:
		TEST_METHOD(QuatMultiply)
		{
			//90 degrees on x
			Quat q = Quat(0.707f, 0.707f, 0.0f, 0.0f);
			Quat q2 = Quat(0.707f, 0.707f, 0.0f, 0.0f);
			q.Multiply(q2);
			q.ToEuler();

			q.ToString();
			
			Assert::AreEqual(Quat(0.0f, 0.999698f, 0.0f, 0.0f), q);
		}

		TEST_METHOD(QuatToEuler)
		{
			Quat q = Quat(0.707f, 0.707f, 0.0f, 0.0f);
			Vec3 v = q.ToEuler();

			Assert::IsTrue(AreEqual(v.x, 90.0f, 1.0f), (L"Got : " + std::to_wstring(v.x) + L" Expected : ~90.0f").c_str());

			q = Quat(0.5f, 0.5f, 0.5f, 0.5f);

			v = q.ToEuler();

			Assert::IsTrue(AreEqual(v.x, 90.0f, 1.0f), (L"Got : " + std::to_wstring(v.x) + L" Expected : ~90.0f").c_str());
			Assert::IsTrue(AreEqual(v.z, 90.0f, 1.0f), (L"Got : " + std::to_wstring(v.z) + L" Expected : ~90.0f").c_str());

			q = Quat(0.5f, 0.5f, -0.5f, -0.5f);

			v = q.ToEuler();

			Assert::IsTrue(AreEqual(v.x, 90.0f, 1.0f), (L"Got : " + std::to_wstring(v.x) + L" Expected : ~90.0f").c_str());
			Assert::IsTrue(AreEqual(v.z, -90.0f, 1.0f), (L"Got : " + std::to_wstring(v.z) + L" Expected : ~-90.0f").c_str());
		}

		TEST_METHOD(QuatFromEuler)
		{
			Quat q = Quat::FromEuler(90.0f, 0.0f, 0.0f);

			Assert::AreEqual(q , Quat(0.707106769f, 0.707106769f, 0.0f, 0.0f));

			q = Quat::FromEuler(90.0f, 0.0f, 90.0f);

			Assert::AreEqual(q, Quat(0.5f, 0.5f, 0.5f, 0.5f));

			q = Quat::FromEuler(90.0f, 0.0f, -90.0f);

			Assert::AreEqual(q, Quat(0.5f, 0.5f, -0.5f, -0.5f));
		}

		TEST_METHOD(QuatVec3Multiply)
		{
			Vec3 pos = Vec3(1.f, 0.f, 0.f);
			Quat q = Quat::FromEuler(0.f, 90.f, 0.f);

			Vec3 result = q*pos;

			Assert::IsTrue(AreEqual(result.x, 0.f, .001f) && AreEqual(result.y, 0.f, .001f)  && AreEqual(result.z, 1.f, .001f));
		
			pos = Vec3(15.f, 0.f, 0.f);
			q = Quat(1.0f, 0.0f, 0.0f, 0.0f);

			result = q*pos;

			Assert::IsTrue(AreEqual(result.x, 15.f, .001f) && AreEqual(result.y, 0.f, .001f) && AreEqual(result.z, 0.f, .001f));

			pos = Vec3(0.f, 0.f, 0.f);
			q = Quat(1.0f, 0.0f, 0.0f, 0.0f);

			result = q*pos;

			Assert::IsTrue(AreEqual(result.x, 0.f, .001f) && AreEqual(result.y, 0.f, .001f) && AreEqual(result.z, 0.f, .001f));

			pos = Vec3(15.f, 0.f, 0.f);
			q = Quat::FromEuler(0.f, 180.f, 0.f);

			result = q*pos;

			Assert::IsTrue(AreEqual(result.x, -15.f, .001f) && AreEqual(result.y, 0.f, .001f) && AreEqual(result.z, 0.f, .001f));

			pos = Vec3(15.f, 15.f, 0.f);
			q = Quat::FromEuler(0.f, 180.f, 0.f);

			result = q*pos;

			Assert::IsTrue(AreEqual(result.x, -15.f, .001f) && AreEqual(result.y, 15.f, .001f) && AreEqual(result.z, 0.f, .001f));

			pos = Vec3(15.f, 15.f, 0.f);
			q = Quat::FromEuler(90.f, 180.f, 0.f);

			result = q*pos;

			Assert::IsTrue(AreEqual(result.x, -15.f, .001f) && AreEqual(result.y, 0.f, .001f) && AreEqual(result.z, -15.f, .001f));
		}

		TEST_METHOD(QuatNormalize)
		{
			Quat q = Quat(0.707f, 0.707f, 0.0f, 0.0f);
			q.w += 0.5f;
			q.x += 0.5f;
			
			q.Normalize();

			Assert::IsTrue(AreEqual(q.x, 0.707f, 0.001f) && AreEqual(q.w, 0.707f, 0.001f));
		}
	};
}

namespace Microsoft
{
namespace VisualStudio
{
namespace CppUnitTestFramework
{

template<>
static std::wstring ToString<Quat>(const Quat& q) 
{
	RETURN_WIDE_STRING(q.ToString().c_str());
}

} // CppUnitTestFramework
} // VisualStudio
} // Microsoft